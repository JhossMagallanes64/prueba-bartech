package com.prueba.bartech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.bartech.dto.PersonaDTO;
import com.prueba.bartech.service.IServiceAPIPerson;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/PersonaAPI")
public class PersonaAPIController {

	@Autowired
	IServiceAPIPerson apiService;
	
	@GetMapping(value="/listarDocument/{dni}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PersonaDTO> obtenerPersonaXDni(@PathVariable("dni")String dni){
		PersonaDTO dtoPerson = new PersonaDTO();
		ResponseEntity<PersonaDTO> respuesta = null;
		try {
			dtoPerson = apiService.obtenerDatosServicioApi(dni);
			respuesta = new ResponseEntity<PersonaDTO>(dtoPerson,HttpStatus.OK);
		} catch (Exception e) {
			respuesta = new ResponseEntity<PersonaDTO>(dtoPerson,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return respuesta;
		
	}
}
