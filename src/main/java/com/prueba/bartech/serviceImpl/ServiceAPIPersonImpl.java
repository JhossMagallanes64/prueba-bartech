package com.prueba.bartech.serviceImpl;

import com.prueba.bartech.dto.PersonaDTO;
import com.prueba.bartech.service.IServiceAPIPerson;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.springframework.stereotype.Service;

@Service
public class ServiceAPIPersonImpl implements IServiceAPIPerson {

	@Override
	public PersonaDTO obtenerDatosServicioApi(String dni) {
		PersonaDTO dtoPerson = new PersonaDTO();
		javax.ws.rs.client.Client client = ClientBuilder.newClient(
        new ClientConfig().register(JacksonJsonProvider.class));
        WebTarget webTarget = client.target("https://dni.optimizeperu.com/api/persons/"+dni);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();
        dtoPerson = response.readEntity(PersonaDTO.class);
        System.out.println("DNI "+dtoPerson.getDni());
		return dtoPerson;
	}

}
