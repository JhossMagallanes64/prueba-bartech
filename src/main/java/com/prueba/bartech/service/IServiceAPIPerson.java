package com.prueba.bartech.service;

import com.prueba.bartech.dto.PersonaDTO;

public interface IServiceAPIPerson {
	
	public PersonaDTO obtenerDatosServicioApi (String dni);

}
